import csv
import sys

from shapes import create_shape


class Statistics:
    def __init__(self):
        self._count = 0
        self._sum = 0
        self.max = 0

    def include(self, value: float) -> None:
        self._count += 1
        self._sum += value
        if value > self.max:
            self.max = value

    def average(self) -> float:
        return self._sum / self._count


def main(argv: list[str]) -> None:
    perimeters = Statistics()
    areas = Statistics()

    with open(argv[1]) as csv_file:
        row_num = 0
        for row in csv.reader(csv_file):
            row_num += 1
            try:
                shape = create_shape(row)
                perimeters.include(shape.perimeter())
                areas.include(shape.area())
            except Exception as e:
                print(f"Line number {row_num} contains problem: {e}")
                sys.exit(1)

    print(f"Max perimeter = {perimeters.max} m")
    print(f"Avg perimeter = {perimeters.average()} m")
    print(f"Max area = {areas.max} m^2")
    print(f"Avg area = {areas.average()} m^2")

if __name__ == '__main__':
    main(sys.argv)
