import sys

from shapes import load_shapes

all_shapes = load_shapes(sys.argv[1])

max_perimeter = max(shape.perimeter() for shape in all_shapes)

print(f"Max perimeter = {max_perimeter} m")
