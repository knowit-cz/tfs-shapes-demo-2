import sys

from shapes import load_shapes

all_shapes = load_shapes(sys.argv[1])

max_area = max(shape.area() for shape in all_shapes)
sum_area = sum(shape.area() for shape in all_shapes)

avg_area = sum_area / len(all_shapes)

print(f"Max area = {max_area} m^2")
print(f"Avg area = {avg_area} m^2")
