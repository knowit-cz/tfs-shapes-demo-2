import sys

from shapes import load_shapes, Shape

def sort_by_function_by_name(shapes: list[Shape], function) -> list[tuple[str, float]]:
    grouped = dict()
    for shape in all_shapes:
        grouped.setdefault(shape.name(), list()).append(shape)

    grouped_areas = {key: map(function, shapes) for key, shapes in grouped.items()}
    grouped_sum_areas = {key: sum(areas) for key, areas in grouped_areas.items()}

    return sorted(grouped_sum_areas.items(), key=lambda tuple: tuple[1], reverse=True)

all_shapes = load_shapes('many_shapes.csv')

for name, area in sort_by_function_by_name(all_shapes, Shape.area):
    print(f'Shapes of type {name} has total area: {area}')

for name, perimeter in sort_by_function_by_name(all_shapes, Shape.perimeter):
    print(f'Shapes of type {name} has total perimeter: {perimeter}')