import unittest
from divide import divide


class DivideTests(unittest.TestCase):
    def test_by_3(self):
        number = 3
        result = divide([1, 2, 3, 4, 5, 6, 7, 8, 9], number)
        self.assertEqual(len(result), number)
        self.assertListEqual(result[0], [1, 4, 7])
        self.assertListEqual(result[1], [2, 5, 8])
        self.assertListEqual(result[2], [3, 6, 9])


if __name__ == '__main__':
    unittest.main()
