import csv
import sys

import shapes

with open(sys.argv[1]) as csv_file:
    row_num = 0
    for row in csv.reader(csv_file):
        row_num += 1
        try:
            shape = shapes.create_shape(row)

            print(f'Shape: {shape.name()}')
            for line in shape.render_lines():
                print(line)
            print('-------------')

        except Exception as e:
            print(f"Line number {row_num} contains problem: {e}")
            sys.exit(1)

