import sys
import math

if len(sys.argv) < 2:
    print("Missing circle radius parameter!")
    sys.exit(1)

try:
    radius = float(sys.argv[1])
except ValueError:
    print(f"Invalid circle radius value: '{sys.argv[1]}'")
    sys.exit(2)

if radius <= 0:
    print(f"Invalid circle radius: {radius}")
    sys.exit(3)

print(f"The circle radius length: {radius} [m]")

perimeter = 2 * math.pi * radius
print(f" P = {perimeter} m")

area = math.pi * radius ** 2
print(f" A = {area} m^2")
