import csv
import math
import abc
import sys


class Shape(abc.ABC):
    def name(self) -> str:
        return type(self).__name__.lower()

    @abc.abstractmethod
    def description(self) -> str:
        return self.description()

    @abc.abstractmethod
    def perimeter(self) -> float:
        return self.perimeter()

    @abc.abstractmethod
    def area(self) -> float:
        return self.area()

    @abc.abstractmethod
    def render_lines(self) -> list[str]:
        return self.render_lines()


class Square(Shape):
    def __init__(self, side: float):
        self._side = side

    def description(self) -> str:
        return f'Square (side={self._side})'

    def perimeter(self) -> float:
        return 4 * self._side

    def area(self) -> float:
        return self._side ** 2

    def render_lines(self) -> list[str]:
        side = int(self._side)
        return ['*  ' * side for _ in range(side)]


class Rectangle(Shape):
    def __init__(self, length: float, width: float):
        self._length = length
        self._width = width

    def description(self) -> str:
        return f'Rectangle (length={self._length}; width={self._width})'

    def perimeter(self) -> float:
        return 2 * (self._length + self._width)

    def area(self) -> float:
        return self._length * self._width

    def render_lines(self) -> list[str]:
        width = int(self._width)
        length = int(self._length)
        return ['*  ' * width for _ in range(length)]


class Circle(Shape):
    def __init__(self, radius: float):
        self._radius = radius

    def description(self) -> str:
        return f'Circle(radius={self._radius})'

    def perimeter(self) -> float:
        return 2 * math.pi * self._radius

    def area(self) -> float:
        return math.pi * self._radius ** 2

    def render_lines(self) -> list[str]:
        radius = int(self._radius)
        area = range(-radius, radius + 1)

        make_point = lambda x, y: '**' if self._is_point_in_circle(x, y) else '  '
        make_line = lambda y: ''.join([make_point(x, y) for x in area])

        return [make_line(y) for y in area]

    def _is_point_in_circle(self, x, y):
        return x ** 2 + y ** 2 <= self._radius ** 2


class Donut(Shape):
    def __init__(self, outer_radius: float, inner_radius: float):
        self._outer_radius = outer_radius
        self._inner_radius = inner_radius

    def description(self) -> str:
        return f'Donut(outer-radius={self._outer_radius}; inner-radius={self._inner_radius})'

    def perimeter(self) -> float:
        return 2 * math.pi * self._outer_radius

    def area(self) -> float:
        return math.pi * (self._outer_radius ** 2 - self._inner_radius ** 2)

    def render_lines(self) -> list[str]:
        radius = int(self._outer_radius)
        area = range(-radius, radius + 1)

        make_point = lambda x, y: '**' if self._is_point_in_circle(x, y) else '  '
        make_line = lambda y: ''.join([make_point(x, y) for x in area])

        return [make_line(y) for y in area]

    def _is_point_in_circle(self, x, y):
        return self._inner_radius ** 2 <= x ** 2 + y ** 2 <= self._outer_radius ** 2


def create_shape(row: list[str]) -> Shape:
    shape_type = row[0]
    shape_parameters = row[1]
    match shape_type:
        case "square":
            return Square(float(shape_parameters))
        case "rectangle":
            params = shape_parameters.split("*")
            return Rectangle(float(params[0]), float(params[1]))
        case "circle":
            return Circle(float(shape_parameters))
        case "donut":
            params = shape_parameters.split("-")
            return Donut(float(params[0]), float(params[1]))
        case _:
            raise NotImplementedError(f"unknown shape type '{shape_type}'")


def load_shapes(file_path: str):
    row_num = 0
    try:
        all_shapes = []
        with open(file_path) as csv_file:
            for row in csv.reader(csv_file):
                row_num += 1
                all_shapes.append(create_shape(row))
        return all_shapes
    except Exception as e:
        print(f"Line number {row_num} contains problem: {e}")
        sys.exit(1)
