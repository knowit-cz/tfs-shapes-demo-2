import sys

if len(sys.argv) < 2:
    print("Missing square side parameter!")
    sys.exit(1)

try:
    side = float(sys.argv[1])
except ValueError:
    print(f"Invalid square side value: '{sys.argv[1]}'")
    sys.exit(2)

if side <= 0:
    print(f"Invalid square side: {side}")
    sys.exit(3)

print(f"The square side length: {side} [m]")

perimeter = 4 * side
print(f" P = {perimeter} m")

area = side ** 2
print(f" A = {area} m^2")
