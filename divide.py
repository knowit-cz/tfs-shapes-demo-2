
def divide(items: list, number: int) -> list[list]:
    return [items[n::number] for n in range(number)]

