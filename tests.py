import sys
from unittest import TestCase
from unittest.mock import patch
from io import StringIO
from contextlib import redirect_stdout

import shapes_statistics


class TestShapes(TestCase):
    """
    Automated test utilizing several StackOverflow suggestions to test our script:
    https://stackoverflow.com/questions/5136611/capture-stdout-from-a-script
    https://stackoverflow.com/questions/5544752/should-i-use-a-main-method-in-a-simple-python-script
    """
    def test_area_script_output(self):
        stdout = StringIO()

        test_args = ['area.py', 'shapes.csv']
        with patch.object(sys, 'argv', test_args), redirect_stdout(stdout):
            import area

        self.assertEqual(stdout.getvalue(), 'Max area = 64.0 m^2\n'
                                            'Avg area = 56.882741228718345 m^2\n')

    def test_perimeter_script_output(self):
        stdout = StringIO()

        test_args = ['perimeter.py', 'shapes.csv']
        with patch.object(sys, 'argv', test_args), redirect_stdout(stdout):
            import perimeter

        self.assertEqual(stdout.getvalue(), 'Max perimeter = 32.0 m\n')

    def test_shapes_script_output(self):
        stdout = StringIO()

        with redirect_stdout(stdout):
            import shapes_statistics

            shapes_statistics.main(['shapes.py', 'shapes.csv'])

        self.assertEqual(stdout.getvalue(), 'Max perimeter = 32.0 m\n'
                                            'Avg perimeter = 30.13716694115407 m\n'
                                            'Max area = 64.0 m^2\n'
                                            'Avg area = 56.882741228718345 m^2\n')
